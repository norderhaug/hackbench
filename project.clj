(defproject hackbench "0.4.2-SNAPSHOT"
  :description "Rapid prototyping in Clojure"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.520"]
                 [org.clojure/core.async "0.4.490"]
                 [cljsjs/react "16.4.0-0"]
                 [cljsjs/react-dom "16.4.0-0"]
                 [cljsjs/react-dom-server "16.4.0-0"]
                 [cljsjs/create-react-class "15.6.3-1"]
                 [sablono "0.8.6"] ;; tmp for material-ui
                 [edpaget/cljs-material-ui "3.9.1-4-SNAPSHOT"
                   :exclusions [sablono
                                org.clojure/clojure
                                org.clojure/clojurescript]]
                 [cljsjs/pubnub "4.1.1-0"]
                 [reagent "0.8.1"]
                 [clj-commons/secretary "1.2.4"]
                 [re-frame "0.10.6"]
                 [com.taoensso/timbre "4.10.0"]
                 [cljs-http "0.1.46"]
                 [com.taoensso/sente "1.13.1"]
                 [camel-snake-kebab "0.4.0"]
                 [org.clojars.grav/hickory "0.7.1-SNAPSHOT"] ; https://github.com/grav/hickory/tree/node
                 [bidi "2.1.6"]
                 [macchiato/core "0.2.15"]
                 [mount "0.1.16"]]

  ; :npm-deps [[cors "2.8.5"]]

  :npm {:write-package-json true
        :dependencies [[source-map-support "0.5.3"]
                       [~(symbol "@cljs-oss/module-deps") "1.1.1"] ;; temporary figwheel missing in compilation, eliminate asap
                       [xmldom "0.1.27"]
                       [jsdom "15.1.0"]
                       [express "4.17.1"]
                       [cors "2.8.5"]
                       [xhr2 "0.2.0"]
                       [react "16.4.0"]
                       [react-dom "16.4.2"]
                       [create-react-class "15.6.3"]
                       [pubnub "4.1.1"]]

        :root :root}

  :plugins [[lein-cljsbuild "1.1.7"]
            [macchiato/lein-npm "0.6.7"]]

  :min-lein-version "2.5.3"

  :hooks [leiningen.cljsbuild]

  :aliases {"start" ["npm" "start"]
            "test" ["with-profile" "test" "doo" "node" "server" "once"]}

  :main "main.js"

  :source-paths ["src/universal"]

  :clean-targets ^{:protect false} [[:cljsbuild :builds :server :compiler :output-to]
                                    [:cljsbuild :builds :app :compiler :output-dir]
                                    "node_modules"
                                    :target-path :compile-path]

  :figwheel {:http-server-root "public"
             :css-dirs ["resources/public/css"]
             :server-logfile "logs/figwheel.log"
             :load-all-builds false
             :builds-to-start [:app :server]}

  :cljsbuild {:builds
              {:app
               {:source-paths ["src/browser" "src/universal" "src/util"]
                :compiler {:output-to "resources/public/js/out/app.js"
                           :output-dir "resources/public/js/out"
                           :asset-path "js/out"
                           :main app.start
                           :optimizations :none}}

               :server
               {:source-paths ["src/node" "src/universal" "src/util"]
                :compiler {:target :nodejs
                           :output-to "main.js"
                           :output-dir "target"
                           :main server.core
                           :foreign-libs [{:file "src/node/polyfill/simple.js"
                                           :provides ["polyfill.simple"]}]
                           :optimizations :none}}}}


  :profiles {:dev
             {:plugins
              [[lein-figwheel "0.5.19-SNAPSHOT"]
               [lein-doo "0.1.11"]]
              :cljsbuild
              {:builds
               {:app
                {:compiler {:pretty-print true
                            :source-map false}
                 :figwheel {:on-jsload "reagent.core/force-update-all"}}
                :server
                {:compiler {:pretty-print true
                            :source-map false}
                 :figwheel {:heads-up-display false}}}}
              :npm {:dependencies [[ws "3.3.3"]]}}

             :test {:cljsbuild
                    {:builds
                     {:server
                      {:source-paths ["test"]
                       :compiler {:main runners.doo
                                  :optimizations :none
                                  :output-to "target/test/server.js"
                                  :output-dir "target/test"}}}}}

             :production
             {:env {:production true}
              :cljsbuild
              {:builds
               {:server
                {:compiler {;:optimizations :simple
                            :pretty-print false}}
                :app
                {:compiler {:output-dir "target/app/out"
                            :optimizations :advanced
                            :pretty-print false}}}}}})
