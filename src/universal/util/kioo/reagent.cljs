(ns util.kioo.reagent
  (:require
   [taoensso.timbre :as timbre]
   [reagent.core :as reagent]
   [hickory.select :as hs]
   [hickory.core :as hickory
    :refer [as-hiccup as-hickory]]
   [hickory.convert :as hc
    :refer [hickory-to-hiccup]]
   [hickory.zip
    :refer [hiccup-zip hickory-zip]]
   [hickory.utils :as utils]
   [clojure.zip :as zip]))

;; TODO: Consider using Specter https://github.com/nathanmarz/specter
;; ALSO: https://github.com/eerohele/sigel

(defn parse-to-hickory [content]
  (with-redefs [;js/Node goog.dom.NodeType
                ;js/NodeList goog.dom.NodeList
                ;js/document (.-document (.-window (jsdom.JSDOM.)))
                hickory.core/Attribute nil]
    (-> content
        (hickory/parse)
        (as-hickory))))


(defn selector-matcher [selector]
  ;; limited ad-hoc selectors for now...
  (let [match? (if (= selector [:#forkme])
                 (hs/child (hs/id "forkme")) ;; ## generalize
                 (apply hs/child (map hs/tag selector)))]
    (fn [loc]
      (let [node (zip/node loc)]
        (and (not (empty? node))
             (not (fn? (:tag node))) ; reagent component
             (match? loc))))))


(defn transform [zipper transformation]
  ;; # bug: changes original tree instead of building new tree,
  ;; so pattern matching will happen on transformed not original
  #_ (timbre/debug "Transform:" transformation)
  (loop [loc zipper]
    (if (zip/end? loc)
      (zip/root loc)
      (if-let [editor (some (fn [[matcher editor]]
                              (if (matcher loc) editor))
                            transformation)]
        (recur (zip/next (editor loc)))
        (recur (zip/next loc))))))

(def default-transform
  {(hs/node-type :comment) zip/remove})

(defn compile-transformation [rules]
  (into default-transform
        (for [[selector action] rules]
          [(selector-matcher selector) action])))

(defn parse-snippet [content path body]
  #_(timbre/info "Snippet:" path body)
  (-> (parse-to-hickory content)
      (hickory-zip)
      (transform (compile-transformation body))
      (hc/hickory-to-hiccup)
      (second) ;; skip doctype
      (reagent/as-element)))

(defn reagent-component [node & nodes]
  (fn [& content]
    #_(timbre/debug "Render custom component:" node nodes content)
    (if (empty? nodes)
      node
     (into [:<> node] nodes))))

(defn z-component [node]
  (if true
    {:type :element
     :tag (reagent-component node)}
    {:type :comment
     :content [(pr-str node)]}))

#_
(-> (page-x {})
    (hickory-to-hiccup))


(defn content [& nodes]
  (fn [loc]
    ;; should also handle missing attributes?
    ; (timbre/debug "Edit:" nodes)
    (zip/edit loc (fn [node]
                    (assoc node
                           :content (map z-component nodes))))))


(defn append [node & nodes]
  (fn [loc]
    (zip/append-child loc (z-component node))
    #_
    (let [appender (fn [loc node]
                     (zip/append-child loc (z-component node)))]
      (reduce appender loc nodes))))
