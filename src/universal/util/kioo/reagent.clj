(ns util.kioo.reagent
  (:require
   [hickory.core :as hickory
     :refer [as-hiccup as-hickory]]))

"Emulate kioo for react 16 compatibility"

(defmacro defsnippet [name file path args body]
  `(defn ~name ~args
     (parse-snippet ~(slurp (str "resources/" file )) ~path ~body)))
