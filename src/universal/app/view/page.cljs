(ns app.view.page
  (:require
   [goog.dom :as dom]
   [util.kioo.reagent
    :refer [#_html-content content append #_after #_set-attr #_do->
            #_substitute #_listen #_unwrap]
    :refer-macros [defsnippet #_deftemplate #_snippet]]
   [taoensso.timbre :as timbre]
   [clojure.zip :as zip]
   [reagent.core :as reagent
    :refer [atom]]
   [reagent.dom.server
    :refer [render-to-string]]
   [goog.string :as gstring]
   [util.lib :include-macros true
    :refer [slurp]]
   [app.mobile.core :as mobile]
   [app.view.view
    :refer [view]]))

(defn script-element [src]
  [:script
     (if (map? src)
       src
       {:dangerouslySetInnerHTML
        {:__html src}})])


(defsnippet page "template.html" [:html]
  [state & {:keys [scripts title forkme]}]
  {[:head :title] (if title (content title) identity)
   [:main] (content [view state])
   [:#forkme] (if forkme identity (content nil))
   [:body] (append [:div (for [[ix src] (map-indexed vector scripts)]
                           ^{:key (str ix)}
                           [script-element src])])})


(defn html5 [content]
  (->> (render-to-string content)
       (str "<!DOCTYPE html>\n")))

#_ ;; generates react component
(-> (page {} :title "Hello"
             :scripts [{:src "/js/out/app.js"}])
    (html5))
