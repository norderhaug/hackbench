(ns app.dashboard.info
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [util.lib :as lib
    :refer [pp-element pp-str]]
   [app.dashboard.pane
    :refer [pane]]))

(defn view [session]
   [lib/pp-table session])
