(ns app.dashboard.about
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [app.dashboard.pane
    :refer [pane]]))

(defn view [session]
  [:div "About"])
