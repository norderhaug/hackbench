(ns polyfill.compat
  (:require
   [cljs.nodejs :as nodejs]
   ; [polyfill.simple]
   [goog.dom :as dom]
   ["react" :as react]
   ["react-dom" :as react-dom]
   ["xhr2" :as xhr2]
   ["xmldom" :as xmldom]
   ["jsdom"
    :refer [JSDOM]]))

"
Load before any module that depends on basic browser context.
You may have to use (.require js/goog) before the ns delcaration as
the cljs compiler results in arbitrary order of required modules.
goog.require is supposed to be synchronous when used under node,
(https://github.com/google/closure-library/wiki/Using-Closure-Library-with-node.js)
but that doesn't seem to be the case, which may require additional concerns.
"

(enable-console-print!)

;;; exporting react symbols is still needed but should be eliminated asap:

(js/goog.exportSymbol "React" react)
(js/goog.exportSymbol "ReactDOM" react-dom)

;; http://stackoverflow.com/questions/8554745/implementing-an-ajax-call-in-clojurescript
;; http://stackoverflow.com/questions/21839156/node-js-javascript-html-xmlhttprequest-cannot-load

;; Required for goog.net.XhrIo on node:

(js/goog.exportSymbol "XMLHttpRequest" xhr2)

;; still needed for loading material-ui on node.

(set! js/window js/global)
(set! js/navigator #js{:useragent nil})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DOM

;; https://github.com/google/closure-library/wiki/Using-Closure-Library-with-node.js
;; "any libraries in Closure Library that use the DOM will not work on NodeJS"
;; https://www.npmjs.com/package/jsdom
;; Taking that as a challenge!

(def dom-parser (if (exists? js/DOMParser)
                  js/DOMParser
                  (case :jsdom
                    :jsdom  (.. (new JSDOM) -window -DOMParser)
                    :xmldom (.-DOMParser xmldom))))

;; Required by hickory.core/parse-dom-with-domparser

(js/goog.exportSymbol "DOMParser" dom-parser)
